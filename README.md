# _WAAW06_ mail-server

毎週1つWebアプリを作ろうの6回目。

期間は18/06/15 - 18/06/21

自力でメールサーバを実装してみようと思う。


## ゴール

* [ ] メールを送信できる
* [ ] メールを受信できる
* [ ] 任意のタイミングで過去に送受信したメールを取得できる


## 目的

* [ ] smtpの勉強
* [ ] popの勉強
* [ ] webサーバと一緒にまとめて実装できるようになると便利


## 課題

* [ ] tcp

    * [ソケット通信 - はじめてのGo言語](http://cuto.unirita.co.jp/gostudy/post/socket/)

* [ ] smtp

    * [RFC5321(Simple Mail Transfer Protocol)](http://srgia.com/docs/rfc5321j.html#p4)
    * [SMTP（Simple Mail Transfer Protocol）～前編：インターネット・プロトコル詳説（5） - ＠IT](http://www.atmarkit.co.jp/ait/articles/0105/02/news001.html)

* [ ] pop
* [ ] 認証


## 振り返り

0. Idea:

    アイデアやテーマについて
    * 良くも悪くもプロトコルの定義を実装するだけなのでアイデアはない

0. What went right:

    成功したこと・できたこと
    * tcpサーバとしてreadしてwriteする
    * tcpサーバとしてconnectionを管理する

0. What went wrong:

    失敗したこと・できなかったこと
    * smtp全般
    * pop全般

0. What I learned:

    学べたこと
    * smtpがどいういった思想でどういったプロトコルなのか


## サンプル

なし
