package main

import (
    "log"
    "net"
    "time"
    "strings"
)

func main() {
    log.Println("start listen")
    l, err := net.Listen("tcp", "0.0.0.0:25")
    if err != nil {
        log.Fatalln(err)
    }
    defer l.Close()

    for {
        log.Println("start accept")
        conn, err := l.Accept()
        if err != nil {
            log.Println(err)
            continue
        }
        conn.Write([]byte("250 OK"))
        go messaging(conn)
    }
}

func messaging(conn net.Conn) {
    defer conn.Close()
    transaction := new(transaction)
    transaction.conn = conn

    log.Println("success connect")
    buf := make([]byte, 1024)
    for {
        n, err := conn.Read(buf)
        if n == 0 {
            log.Println("connection end")
            break
        }
        if err != nil {
            log.Println(err)
        } else {
            message := string(buf[:n])
            args := strings.Split(message, " ")

            command := ""
            arg := ""
            if len(args) >= 2 {
                command = args[0]
                arg = strings.Join(args[1:], " ")
            } else if len(args) == 1 {
                command = args[0]
            } else {
                continue
            }

            transaction.exec(command, arg)
        }
    }
}

func init() {
    // logの出力項目追加
    log.SetFlags(log.LstdFlags | log.Lshortfile)

    // locationの設定
    const location = "Asia/Tokyo"
    loc, err := time.LoadLocation(location)
    if err != nil {
        loc = time.FixedZone(location, 9*60*60)
    }
    time.Local = loc
}

type transaction struct {
    conn net.Conn
    isHello bool
}

func (s *transaction) exec(command, arg string) {
    command = strings.ToUpper(command) // commandは全て大文字にして処理する

    log.Println("command: (" + command + ")")
    log.Println("arg: (" + arg + ")")

    switch command {
    case "EHLO": fallthrough
    case "HELO":
        s.conn.Write([]byte("250 OK"))
        s.isHello = true
    case "MAIL":
    case "RCPT":
    case "DATA":
    }
}
